//
//  JMFMainViewController.m
//  Camera
//
//  Created by José Manuel Fierro Conchouso on 24/02/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import "JMFMainViewController.h"

@interface JMFMainViewController ()

@end

@implementation JMFMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)takePhotoButton:(id)sender {
    
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.showsCameraControls = YES;
    picker.allowsEditing = YES;
    picker.delegate = self;
    
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *originalPhoto = [info objectForKey:UIImagePickerControllerOriginalImage];
    self.image.image = originalPhoto;
    
    UIImageWriteToSavedPhotosAlbum(originalPhoto, nil, NULL, NULL);
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

@end
