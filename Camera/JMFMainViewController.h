//
//  JMFMainViewController.h
//  Camera
//
//  Created by José Manuel Fierro Conchouso on 24/02/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMFMainViewController : UIViewController <UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *image;
- (IBAction)takePhotoButton:(id)sender;

@end
